module NetworkAlarms
  class API < Grape::API
    format :json

    prefix 'tropic/adapter/v1/network'

    resource :alarms do
      get "/" do
        alarm_category_service = AlarmCategoryService.new
        alarm_service = AlarmService.new
        start_time = params[:start_time] || Date.today.prev_year.to_time.to_i
        end_time = params[:end_time] || Time.now.to_i

        categories = alarm_category_service.query_with_alarms({ startAlarmTime: start_time, endAlarmTime: end_time })

        groups = categories.group_by do |category|
          category[:base_class]
        end

        groups.map do |categories|
          categories_value = categories[1]
          base_class_count = 0
          base_class = {count: 0, details: []}
          categories_value.map do |category|
            base_class_count += category[:count]
            detail = {descr: category[:sub_desc], count: category[:count], items: []}
            detail[:items] = category[:alarms].map do |alarm|
              { device_id: alarm['deviceId'], device_name: alarm['deviceName'], url: alarm['alarmDetail']}
            end
            base_class[:details] << detail
            base_class[:type] = category[:base_desc]
          end
          base_class[:count] = base_class_count
          base_class
        end
      end
    end
  end
end
