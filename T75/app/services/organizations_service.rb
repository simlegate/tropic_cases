class OrganizationsService

  def find identifier
    organizations_repository.select do |organ|
      organ.id == identifier
    end.first
  end

  private
  def organizations_repository
    organizations = []
    o1 = Organization.new
    o1.id = 1
    o1.name = '蜀南加气站'
    organizations << o1
    o2 = Organization.new
    o2.id = 2
    o2.name = '蜀南加水站'
    organizations << o2
    o3 = Organization.new
    o3.id = 3
    o3.name = '蜀南加油站'
    organizations << o3
    organizations
  end
end
