class PostsService

  def find identifier
    posts_repository.select do |post|
      post.id == identifier
    end.first
  end

  def query_by_organ_id organ_id
    posts_repository.select do |post|
      post.organization_id == organ_id
    end
  end

  private
  def posts_repository
    posts = []
    p1 = Post.new
    p1.id = 1
    p1.name = '加气岗'
    p1.organization_id = 1
    posts << p1
    p2 = Post.new
    p2.id = 2
    p2.name = '加水岗'
    p2.organization_id = 1
    posts << p2
    p3 = Post.new
    p3.id = 3
    p3.name = '加气岗'
    p3.organization_id = 2
    posts << p3
    p4 = Post.new
    p4.id = 4
    p4.name = '加水岗'
    p4.organization_id = 2
    posts << p4
    p4 = Post.new
    p4.id = 5 
    p4.name = '加气岗'
    p4.organization_id = 3
    posts << p4
    p5 = Post.new
    p5.id = 6
    p5.name = '加水岗'
    p5.organization_id = 3
    posts << p5
    posts
  end
end
