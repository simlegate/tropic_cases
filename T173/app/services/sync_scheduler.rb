require 'rufus-scheduler'
class SyncScheduler

  attr_reader :period

  def initialize period
    @period = period
  end

  def execute
    scheduler = Rufus::Scheduler.new

    scheduler.every period do
      LOGGER.debug 'start executing sync scheduler'
      begin
        activity = SyncActivity.order_by(created_at: :desc).first
        start_sync_time = activity && activity.end_date
        LOGGER.debug 'the start sync time at ' + start_sync_time.to_s
        source = SchoolSource.new start_sync_time
        updater = SchoolUpdater.new source
        updater.execute
      rescue => exception
        LOGGER.error(exception)
        raise
      end
      LOGGER.debug 'complete sync scheduler'
    end

    scheduler.join
  end
end
