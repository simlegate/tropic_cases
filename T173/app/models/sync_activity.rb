class SyncActivity
  include Mongoid::Document
  include Mongoid::Timestamps

  field :start_date, type: String
  field :added_class_numbers, type: Integer
  field :added_student_numbers, type: Integer
  field :end_date, type: String

end
