#!/usr/bin/env puma

application_path = '/opt/app'
directory application_path
environment 'production'
threads 1, 6
preload_app!

bind "unix:///tmp/puma.sock"
pidfile "#{application_path}/tmp/pids/puma_9292.pid"
state_path "#{application_path}/tmp/pids/puma_9292.state"
stdout_redirect "#{application_path}/logs/puma_9292.stdout.log", "#{application_path}/logs/puma_9292.stderr.log"

port 9292
workers 1
