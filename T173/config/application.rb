require 'pathname'
require 'mongoid'
require 'dotenv'
require 'lumberjack'

ENV['RUNNING_ENV'] ||= 'development'

unless ['development', 'production'].include? ENV['RUNNING_ENV']
  raise "unsupported [#{ENV['RUNNING_ENV']}] env"
end

Dotenv.load(File.expand_path("../#{ENV['RUNNING_ENV']}.env",  __FILE__))

Mongoid.load!('config/mongoid.yml', ENV['RUNNING_ENV'])

APP_ROOT = Pathname.new(Dir.pwd)

Dir[APP_ROOT.join('app', 'models', '*.rb')].each { |file| require file }
Dir[APP_ROOT.join('app', 'services', '*.rb')].each { |file| require file }

if 'development'.eql? ENV['RUNNING_ENV']
  LOGGER = Lumberjack::Logger.new
  LOGGER.level = :debug
else
  LOGGER = Lumberjack::Logger.new("logs/#{ENV['RUNNING_ENV']}.log")
  LOGGER.level = :debug
end
