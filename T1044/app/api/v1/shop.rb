require 'date'

module API
  class Shop < Grape::API
    format :json

    helpers do
      include DivisionHelper

      def validate_msid
        shop = SEQ_BI_DB[:t_shopssp][ms_id: params[:MSid]]
        error!({resultCode: 404, resultMsg: "未找到", contents: []}, 404) unless shop
        shop
      end

      def validate_date(d)
        if d == 'today'
          Date.today
        elsif d == 'yesterday'
          Date.today - 1
        else d
          begin
            Date.parse(d)
          rescue => e
            error!({resultCode: 470, resultMsg: "参数错误", contents: []})
          end
        end
      end

      def validate_type
        if[ 'salesAmount', 'orderCount',
            'goodsCount', 'buyerCount', 'perTicketAmount' ].include?(params[:type])
            params[:type]
        else
          error!({resultCode: 470, resultMsg: "参数错误", contents: []})
        end
      end

      def validate_unit
        if params[:unit] == 'day'
          1
        elsif params[:unit] == 'week'
          7
        elsif params[:unit] == 'month'
          30
        else
          error!({resultCode: 470, resultMsg: "参数错误", contents: []})
        end
      end

      def validate_count
        if params[:count] <= 0
          error!({resultCode: 470, resultMsg: "参数错误", contents: []})
        else
          params[:count]
        end
      end

      def validate_kind
        unless params[:kind] == 'goods'
          error!({resultCode: 470, resultMsg: "参数错误", contents: []})
        else
          params[:kind]
        end
      end

      def validate_top
        if params[:top]  == 'all'
          params[:top]
        elsif params[:top].to_i > 0
          params[:top].to_i
        else
          error!({resultCode: 470, resultMsg: "参数错误", contents: []})
        end
      end

      def exception_handling &blk
        contents = blk.call

        begin
          {
            "resultCode": 200,
            "resultMsg": "接口请求成功",
            "contents": contents,
          }
        rescue => _
          {
            "resultCode": 570,
            "resultMsg": "系统异常",
            "contents": [],
          }
        end
      end
    end

    desc '商家关键指标'
    params do
      requires :MSid, type: Integer, desc: '商家ID'
      requires :date, type: String, desc: '日期； today表示当天，yesterday表示昨日，
        否则就是一个具体的格式日期(yyyy-mm-dd).'
    end
    get '/ShopKeyParas' do
      shop = validate_msid
      date = validate_date(params[:date])
      shop_id = shop[:ms_id]
      exception_handling do
        fetcher = ShopKpiFetcher.new shop_id, date.beginning_of_day, date.end_of_day
        chain_fetcher = ShopKpiFetcher.new shop_id, (date - 1.day).beginning_of_day, (date - 1.day).end_of_day
        fetcher.key_kpis.map do |k, v|
          chain_v = chain_fetcher.key_kpis[k]
          chain = gracefully_divide(v - chain_v, chain_v)*100
          { name: I18n.t(k), detail: v, chainIndex: chain}
        end
      end
    end

    desc '商家关键指标扩展'
    params do
      requires :MSid, type: Integer, desc: '商家ID'
      requires :date, type: String, desc: '日期； today表示当天，yesterday表示昨日，
        否则就是一个具体的格式日期(yyyy-mm-dd).'
    end
    get '/ShopKeyParaSubs' do
      shop = validate_msid
      date = validate_date(params[:date])
      shop_id = shop[:ms_id]
      contents = []
      exception_handling do
        fetcher = ShopKpiFetcher.new shop_id, (date - 7).beginning_of_day, date.end_of_day
        active_kpi = ShopActiveKpi.new(shop_id, (date - 30).beginning_of_day, date.end_of_day)
        contents << { name: "成交额前七日均值", detail: gracefully_divide(fetcher.key_kpis['salesAmount'], 7) }
        contents << { name: "成交数前七日均值", detail: gracefully_divide(fetcher.key_kpis['orderCount'], 7) }
        contents << { name: "30天活跃用户数", detail: active_kpi.value[:active_users_number] }
        contents << { name: "30天活跃商品数", detail: active_kpi.value[:active_goods_number] }
        contents
      end
    end

    desc '商家累计指标'
    params do
      requires :MSid, type: Integer, desc: '商家ID'
      requires :date, type: String, desc: '日期； today表示当天，yesterday表示昨日，
        否则就是一个具体的格式日期(yyyy-mm-dd).'
    end
    get '/ShopKeyParaTotals' do
      shop = validate_msid
      date = validate_date(params[:date])
      shop_id = shop[:ms_id]
      current_time = date.to_time
      fetcher = ShopKpiFetcher.new shop_id, current_time.beginning_of_month, current_time.end_of_day
      chain_fetcher = ShopKpiFetcher.new shop_id, (current_time - 1.month).beginning_of_month, (current_time - 1.month).to_time

      exception_handling do
        fetcher.key_kpis.map do |k, v|
          chain_v = chain_fetcher.key_kpis[k]
          chain = gracefully_divide(v - chain_v, chain_v)*100
          { name: I18n.t(k), detail: v, chainIndex: chain}
        end
      end
    end

    desc '商家关键指标趋势图'
    params do
      requires :unit, type: String, desc: '取订单清单的日期段单位, day | week | month'
      requires :count, type:  Integer, desc: '日期段个数，就是按original为起始日期，
        取count个unit日期段单位确定的所有日期内的订单清单数据.'
      requires :original, type: String, desc: '订单清单的日期起点, today | yesterday | yyyy-mm-dd'
      requires :MSid, type: Integer, desc: '商家ID'
      requires :type, type: String, desc: 'salesAmount | orderCount |
        goodsCount | buyerCount | perTicketAmount'
    end
    get '/ShopKeyTrendlines' do
      shop = validate_msid
      original = validate_date(params[:original])
      days = validate_unit
      count = validate_count
      end_time = original.to_time
      shop_id = shop[:ms_id]
      contents = []
      (days * count).times do |h|
        fetcher = ShopKpiFetcher.new shop_id, end_time-h.days, end_time-h.days+1.day
        time = (end_time-h.days).strftime("%m-%d")
        contents = contents.push({name: time, detail: fetcher.key_kpis[params[:type]]})
      end

      exception_handling do
        [{targetName: I18n.t(params[:type]), data: contents}]
      end
    end

    desc '商家订单来源（终端）排行榜'
    params do
      requires :MSid, type: Integer, desc: '商家ID'
      requires :date, type: String, desc: '日期； today表示当天，yesterday表示昨日，
        否则就是一个具体的格式日期(yyyy-mm-dd).'
    end
    get '/ShopRankOrderSrcs' do
      shop = validate_msid
      date = validate_date(params[:date])
      shop_id = shop[:ms_id]
      current_time = date.to_time
      kpis = ShopKpiFetcher.new shop_id, current_time.beginning_of_day, current_time.end_of_day

      kpis.by_origins_include_rank

      exception_handling do
        kpis.by_origins_include_rank
      end
    end

    desc '商家其他排行榜'
    params do
      requires :MSid, type: Integer, desc: '商家ID'
      requires :date, type: String, desc: '日期； today表示当天，yesterday表示昨日，
        否则就是一个具体的格式日期(yyyy-mm-dd).'
      requires :kind, type: String, desc: '商品, goods'
      requires :top, type: String, desc: '取上面的kind的top多少 例如:all, 5, 10等,all 表示取出所有排行榜，比如订单来源，5个来源全列出'
    end
    get '/ShopRanks' do
      shop = validate_msid
      date = validate_date(params[:date])
      kind = validate_kind
      top = validate_top
      shop_id = shop[:ms_id]

      end_time = date.to_time
      start_time = (date - top.days).to_time

      kpi = SingleShopKpi.new shop_id, start_time, end_time

      exception_handling do
        kpi.include_rank
      end
    end


  end
end
