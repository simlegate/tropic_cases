module ApiHelpers
  def validate_msid
    shop = SEQ_BI_DB[:t_shopssp][ms_id: params[:MSid]]
    error!({resultCode: 404, resultMsg: "未找到", contents: []}, 404) unless shop
    shop
  end

  def validate_date(d)
    if d == 'today'
      Date.today
    elsif d == 'yesterday'
      Date.today - 1
    else d
      begin
        Date.parse(d)
      rescue => e
        error!({resultCode: 470, resultMsg: "参数错误", contents: []})
      end
    end
  end

  def validate_type
    if[ 'salesAmount', 'orderCount',
        'goodsCount', 'buyerCount', 'perTicketAmount' ].include?(params[:type])
        params[:type]
    else
      error!({resultCode: 470, resultMsg: "参数错误", contents: []})
    end
  end

  def validate_unit
    if params[:unit] == 'day'
      1
    elsif params[:unit] == 'week'
      7
    elsif params[:unit] == 'month'
      30
    else
      error!({resultCode: 470, resultMsg: "参数错误", contents: []})
    end
  end

  def validate_count
    if params[:count] <= 0
      error!({resultCode: 470, resultMsg: "参数错误", contents: []})
    else
      params[:count]
    end
  end

  def validate_kind
    unless params[:kind] == 'goods'
      error!({resultCode: 470, resultMsg: "参数错误", contents: []})
    else
      params[:kind]
    end
  end

  def validate_top
    if params[:top]  == 'all'
      params[:top]
    elsif params[:top].to_i > 0
      params[:top].to_i
    else
      error!({resultCode: 470, resultMsg: "参数错误", contents: []})
    end
  end

  def exception_handling &blk
    contents = blk.call

    begin
      {
        "resultCode": 200,
        "resultMsg": "接口请求成功",
        "contents": contents,
      }
    rescue => _
      {
        "resultCode": 570,
        "resultMsg": "系统异常",
        "contents": [],
      }
    end
  end
end
