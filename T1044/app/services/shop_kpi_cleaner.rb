class ShopKpiCleaner

  def self.clean_shop_between shop_id, start_time, end_time
    executed_start_time = Time.now
    kpi = SingleShopKpi.new(shop_id, start_time, end_time).value
    if kpi.present?
      redis_key = "SHOP-#{shop_id}-#{start_time.to_i / 3600}"
      REDIS_CLIENT.hset(redis_key, 'salesAmount', kpi[:salesAmount] || 0)
      REDIS_CLIENT.hset(redis_key, 'buyerCount', kpi[:buyerCount])
      REDIS_CLIENT.hset(redis_key, 'orderCount', kpi[:orderCount])
      REDIS_CLIENT.hset(redis_key, 'byOrigin', kpi[:byOrigin].to_json)
      REDIS_CLIENT.hset(redis_key, 'goodsCount', kpi[:goodsCount] || 0)
      REDIS_CLIENT.hset(redis_key, 'executed_start_time', executed_start_time.to_i)
      REDIS_CLIENT.hset(redis_key, 'executed_end_time', Time.now.to_i)
    end
  end

  def self.clean_shop shop_id, start_time=nil
    unless start_time
      shop_orders = SEQ_BI_DB[:t_orderinfosp].where(ms_id: shop_id, completed: 1).order(:ppl_condate)
      start_time = Time.at(shop_orders.first[:ppl_condate])
    end
    start_time = start_time.beginning_of_hour
    while((start_time + 1.hours) <= Time.now.beginning_of_hour)
      clean_shop_between(shop_id, start_time, (start_time + 1.hours))
      start_time += 1.hours
    end
  end

  def self.clean_all_shops_one_day
    p Time.now.beginning_of_day
    shop_ids.each { |shop_id| clean_shop shop_id, Time.now.beginning_of_day }
  end

  def self.clean_all_shops
    shop_ids.each { |shop_id| clean_shop shop_id }
  end

  def self.clean_all_shops_bewteen start_time, end_time
    shop_ids.each { |shop_id| clean_shop_between(shop_id, start_time, end_time) }
  end

  def self.shop_ids
    SEQ_BI_DB[:t_shopssp].select(:ms_id).map{|v|v[:ms_id]}
  end
end
