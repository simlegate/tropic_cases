class ShopKpiFetcher

  include DivisionHelper

  ORIGIN =  ["WB", "AD", "IO", "WP", "OO", ""]

  attr_reader :shop_id, :start_time, :end_time

  def initialize shop_id, start_time, end_time
    @shop_id = shop_id
    @start_time = start_time
    @end_time = end_time
  end

  def key_kpis
    return @key_kpis if @key_kpis
    kpis = REDIS_CLIENT.multi do
      redis_key.each do |key|
        REDIS_CLIENT.hgetall(key)
      end
    end
    @key_kpis = summary(kpis)
  end

  def by_origins

    s_time = start_time.beginning_of_day.to_i
    e_time = end_time.end_of_day.to_i

    kpis = (s_time..e_time).step(1.hour).map do |timestamp|
      value = REDIS_CLIENT.hgetall("SHOP-#{shop_id}-#{timestamp / 3600}")
      value["byOrigin"] = JSON.parse(value["byOrigin"] || "{}")
      value
    end

    total_amount = total_amount(kpis)
    total_order_count = cal_total_order_count(kpis)

    list = cal_sort_list(kpis, total_amount, total_order_count)

    list
  end


  def by_origins_include_rank
    list = by_origins
    last_list = last_by_origin

    list.each do |l|
      last_list.each do |ll|
        if ll["id"] == l["id"]
          l["amountLastRank"] = ll["amountLastRank"]
          l["countLastRank"] = ll["countLastRank"]
        end
      end
    end

    list
  end

  def last_by_origin
    sf = ShopKpiFetcher.new(shop_id, start_time-1.day, end_time-1.day)
    list = sf.by_origins
    sf.sort_amount_and_count(list)
    list
  end

  def sort_amount_and_count(list)
    list.sort! { |x, y| y["salesAmount"] <=> x["salesAmount"] }.each_with_index do |item, index|
      list[index]["amountLastRank"] = index + 1
    end

    list.sort! { |x, y| y["orderCount"] <=> x["orderCount"] }.each_with_index do |item, index|
      list[index]["countLastRank"] = index + 1
    end
  end

  private

  def redis_key
    s_time = start_time.beginning_of_hour.to_i
    e_time = end_time.end_of_hour.to_i
    (s_time..e_time).step(1.hour).map do |timestamp|
      "SHOP-#{shop_id}-#{timestamp / 3600}"
    end
  end

  def summary kpis
    keys = ['salesAmount', 'orderCount', 'goodsCount']
    summary = keys.inject({}) do |summary, key|
      summary.merge!({ key => kpis.map{|kpi| kpi[key].to_f}.inject(0){|sum,x| sum + x }})
    end
    summary.merge!({ "perTicketAmount" => gracefully_divide(summary["salesAmount"], summary["orderCount"]) })
    summary.merge!({ "buyerCount" => buyer_count })
  end

  def buyer_count
    kpi = SingleShopKpi.new shop_id, start_time, end_time
    kpi.compute[:buyerCount]
  end

  def cal_sort_list(kpis, total_amount, total_order_count)
    ORIGIN.map do |origin|

      r = kpis.inject({}) do |s, e|
        s = s.merge({"buyerCount" => e["byOrigin"][origin]["buyerCount"].to_f + s["buyerCount"].to_f})
        s = s.merge({"salesAmount" => e["byOrigin"][origin]["salesAmount"].to_f + s["salesAmount"].to_f})
        s.merge({"orderCount" => e["byOrigin"][origin]["orderCount"].to_f + s["orderCount"].to_f})
      end

      amountProportion = (r["salesAmount"] / (total_amount.zero? ? 1 : total_amount)).round(4) * 100
      countProportion = (r["orderCount"] / (total_order_count.zero? ? 1 : total_order_count)).round(4) * 100

      r.merge({"id" => origin, "name" => origin, "amountProportion" => amountProportion, "countProportion" => countProportion})
    end
  end

  def cal_total_amount(kpis)
    total_amount = kpis.map do |e|
      ORIGIN.map do |c|
        e["byOrigin"][c]["salesAmount"].to_f
      end.inject &:+
    end.inject &:+
  end

  def cal_total_order_count(kpis)
    kpis.map do |e|
     ORIGIN.map do |c|
       e["byOrigin"][c]["orderCount"].to_f
     end.inject &:+
   end.inject &:+
  end
end
