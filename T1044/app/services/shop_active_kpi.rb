class ShopActiveKpi

  attr_reader :shop_id, :start_time, :end_time

  def initialize shop_id, start_time, end_time
    @shop_id = shop_id
    @start_time = start_time
    @end_time = end_time
  end

  def value
    @active_kpi ||= SEQ_BI_DB[:t_orderinfosp]
      .join(:t_ordergoodssp, :ooi_id => :ooi_id).select{[
      count(distinct(:mb_id)).as(:active_users_number),
      count(distinct(:gg_id)).as(:active_goods_number)]}
      .where(ppl_condate: start_time.to_i..end_time.to_i)
      .where(ms_id: shop_id, completed: 1).first
  end
end
