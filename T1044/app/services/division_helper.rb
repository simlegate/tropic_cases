module DivisionHelper

  def gracefully_divide v1, v2, options={}
    options[:default] ||= 0
    options[:round] ||= 2
    return options[:default] if v2.to_i == 0
    (v1 / v2).round(options[:round]) rescue options[:default]
  end
end
