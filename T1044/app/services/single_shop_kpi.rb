class SingleShopKpi

  attr_reader :shop_id, :start_time, :end_time

  def initialize shop_id, start_time, end_time
    @shop_id = shop_id
    @start_time = start_time
    @end_time = end_time
  end

  def value
    byOrigin = ShopKpiFetcher::ORIGIN.inject({}) { |s, e| s.merge({e => compute(e)}) }
    compute.merge({byOrigin: byOrigin})
  end

  def sales_amount_and_order_count_list
    rs = SEQ_BI_DB[:t_orderinfosp]
      .join(:t_ordergoodssp, :ooi_id => :ooi_id)
      .select{[
        distinct(:gg_id).as(:id),
        sum(:ooi_sum).as(:salesAmount),
        count(:t_orderinfosp__ooi_id).as(:orderCount)]}
      .where(ppl_condate: start_time.to_i..end_time.to_i)
      .where(ms_id: shop_id, completed: 1)
      .group(:gg_id)

    total_amount = rs.inject(0) { |s, e| e[:salesAmount] + s}
    total_order_count = rs.inject(0) { |s, e| e[:orderCount] + s}

    rs.map do |r|
      amountProportion = r[:salesAmount] / total_amount
      countProportion = r[:orderCount] / total_order_count
      r[:amountProportion] = amountProportion.round(4) * 100
      r[:countProportion] = countProportion.round(4) * 100

      good = SEQ_BI_DB[:t_goodsinfosp][gg_id: r[:id]]
      r[:name] = good[:gg_name]
      r
    end
  end

  def include_rank
    list = sales_amount_and_order_count_list
    llist = last_list

    list.each do |l|
      llist.each do |ll|
        if ll[:id] == l[:id]
          l[:amountLastRank] = ll[:amountLastRank]
          l[:countLastRank] = ll[:countLastRank]
        end
      end
    end

    list[0..4]
  end

  def last_list
    last = SingleShopKpi.new shop_id, start_time-1.day, end_time-1.day
    list = last.sales_amount_and_order_count_list

    last.sort_amount_and_count(list)
    list
  end

  def sort_amount_and_count(list)
    list.sort! { |x, y| y[:salesAmount] <=> x[:salesAmount] }.each_with_index do |item, index|
      list[index][:amountLastRank] = index + 1
    end

    list.sort! { |x, y| y[:orderCount] <=> x[:orderCount] }.each_with_index do |item, index|
      list[index][:countLastRank] = index + 1
    end
  end

  def compute(origin=nil)
    orders = SEQ_BI_DB[:t_orderinfosp].select{[
      sum(:ooi_sum).as(:salesAmount),
      count(distinct(:mb_id)).as(:buyerCount),
      count(:ooi_id).as(:orderCount)]}
      .where(ppl_condate: start_time.to_i..end_time.to_i)
      .where(ms_id: shop_id, completed: 1)

    order_goods = SEQ_BI_DB[:t_orderinfosp]
      .join(:t_ordergoodssp, :ooi_id => :ooi_id).select{[
      sum(:oog_amount).as(:goodsCount)]}
      .where(ppl_condate: start_time.to_i..end_time.to_i)
      .where(ms_id: shop_id, completed: 1)


    if origin
      orders = orders.where(Sequel.like(:ooi_code, "#{origin}%"))
      order_goods = order_goods.where(Sequel.like(:ooi_code, "#{origin}%"))
    end

    orders.first.merge order_goods.first
  end

end
