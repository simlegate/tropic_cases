require 'rufus-scheduler'

class ShopKpiWorker
  def perform
    scheduler.every '1h', first_at: Time.now + 1.minute do
      ShopKpiCleaner.clean_all_shops_one_day
    end

    scheduler.join
  end

  def scheduler
    return @scheduler if @scheduler
    @scheduler = Rufus::Scheduler.new
    @scheduler.stderr = File.open('log/scheduler_err.log', 'ab')
    @scheduler
  end
end
