require 'grape'
require 'sequel'
require 'mysql2'
require 'i18n'

ENV['RUNNING_ENV'] ||= 'development'

unless ['development', 'production'].include? ENV['RUNNING_ENV']
  raise "unsupported [#{ENV['RUNNING_ENV']}] env"
end

if ENV['RUNNING_ENV'].eql?('development')
  require 'pry'
end

APP_ROOT = Pathname.new(Dir.pwd)

require File.expand_path('../redis', __FILE__)

I18n.load_path = Dir["#{APP_ROOT}/config/zh-CN.yml"]
I18n.default_locale = "zh-CN"

SEQ_BI_DB = Sequel.connect(YAML.load_file("#{APP_ROOT}/config/database.yml")[ENV['RUNNING_ENV']])

Dir[APP_ROOT.join('app', 'services', '*.rb')].each { |file| require file }
Dir[APP_ROOT.join('app', 'workers', '**/*.rb')].each { |file| require file }
Dir[APP_ROOT.join('app', 'api', '**/*.rb')].each { |file| require file }
