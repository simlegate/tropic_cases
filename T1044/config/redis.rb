require 'redis'

redis_config = YAML.load_file("#{APP_ROOT}/config/redis.yml")[ENV['RUNNING_ENV']]

REDIS_CLIENT = Redis.new(redis_config)
