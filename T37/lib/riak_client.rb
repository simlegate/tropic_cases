require 'riak'
class RiakClient

  attr_reader :source

  def initialize
    @source = Riak::Client.new host: ENV['RIAK_HOST'],
                               pb_port: ENV['RIAK_PORT'],
                               protocol: 'https',
                               ssl: false
  end
end
