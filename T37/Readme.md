# 创建镜像

```
docker build -t --rm tropic_cases:T37 .
```

# 环境变量

* RIAK_HOST: Riak 主机，默认值 127.0.0.1
* RIAK_PORT: Riak 端口，默认值 8098
* 数据文件指定为 /opt/data/data.xls，可以将文件命名为 data.xls，然后将相当的目录挂载到容器中

# 运行示例

```
cp example/data.xls data.xls
docker run -it --rm -v `pwd`:/opt/data -e RIAK_HOST="127.0.0.1" tropic_cases:T37 ruby /opt/client.rb
```
